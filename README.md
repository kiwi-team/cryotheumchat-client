# README #

This README is for info about Kiwi Team's Chatting Service; CryotheumChat!

### What is this repository for? ###

* The CryotheumChat Client that connects you to the world of chatting!

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies

### Who do I talk to? ###

* creator@kiwiteam.org
* kiwiteam.org/index.html/about

CryotheumChat
=

CryotheumChat is a Kiwi Team Chatting service! We will work hard to maintain it for a long time to come!


CryotheumChat Server
=

This is CryotheumChat Server, which is used to give clients access to chats! It will be made public soon!

CryotheumChat Client
=

CryotheumChat Client is what people use to connect to the CryotheumChat server!